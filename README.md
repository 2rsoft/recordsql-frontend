RecordSQL.Frontend
==============

Description
----------

Frontend for object-relational framework RecordSQL (Record v2)
Based on VueJS and Quasar Framework

Instruction
----------
### Prepare
Use ```yarn``` or ```npm install``` for pull node_modules

### Run
Use ```quasar dev```

### Build
Use ```quasar build```
Chack path \dist\spa